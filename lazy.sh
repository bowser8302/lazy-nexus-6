#/bin/bash
#
#
wget https://dl.google.com/dl/android/aosp/shamu-ngi77b-factory-5cd75e2a.zip
unzip shamu-ngi77b-factory-5cd75e2a.zip
cd shamu-ngi77b/
fastboot flash recovery recovery.img
fastboot flash bootloader bootloader-shamu-moto-apq8084-72.04.img 
fastboot reboot bootloader
fastboot flash system system.img
fastboot flash boot boot.img
fastboot flash radio radio-shamu-d4.01-9625-05.51+fsg-9625-02.118.img
fastboot format userdata
fastboot format cache
fastboot reboot bootloader
echo "Device Provisioned. Add provision annotation."
fastboot devices